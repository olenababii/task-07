package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringJoiner;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private static Connection connection;

	public static synchronized DBManager getInstance() {
		if(instance == null) {
			return new DBManager();
		}
		return instance;
	}

	private DBManager() {
		try {
			connection = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		Statement ps;
		ResultSet rs;
		List<User> users = new ArrayList<>();
		try {
			ps = connection.createStatement();
			rs = ps.executeQuery("SELECT * FROM users");
			while (rs.next()) {
				User user = new User();
				users.add(user);
				user.setId(rs.getInt(1));
				user.setLogin(rs.getString(2));
			}
		} catch (Exception e) {
			System.out.println("find all users:" + e.getMessage());
			throw new DBException();
			//return Collections.emptyList();
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		ResultSet id;
		try {
			PreparedStatement statement =
					connection.prepareStatement("INSERT into users VALUES(DEFAULT , ?)", Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, user.getLogin());
			if (statement.executeUpdate() != 1) {
				return false;
			}
			id = statement.getGeneratedKeys();
			if (id.next()) {
				int idField = id.getInt(1);
				user.setId(idField);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException();
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try {
			for (User user: users) {
				PreparedStatement statement = connection.prepareStatement("DELETE FROM users WHERE id = ?");
				statement.setInt(1, user.getId());
				if(statement.executeUpdate() == 0)
					return false;
			}
			for (User user: users) {
				PreparedStatement statement = connection.prepareStatement("DELETE FROM users_teams WHERE user_id = ?");
				statement.setInt(1, user.getId());
				if(statement.executeUpdate() == 0)
					return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException();
		}
		return false;
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM users WHERE login = (?)");
			statement.setString(1, login);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				user.setLogin(resultSet.getString(2));
				user.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException();
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT * FROM teams WHERE name = (?)");
			statement.setString(1, name);
			ResultSet resultSet = statement.executeQuery();
			if(resultSet.next()) {
				team.setName(resultSet.getString(2));
				team.setId(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException();
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		Statement ps;
		ResultSet rs;
		List<Team> teams = new ArrayList<>();
		try {
			ps = connection.createStatement();
			rs = ps.executeQuery("SELECT * FROM teams");
			while (rs.next()) {
				Team team = new Team();
				teams.add(team);
				team.setId(rs.getInt(1));
				team.setName(rs.getString(2));
			}
		} catch (Exception e) {
			System.out.println("find all users:" + e.getMessage());
			throw new DBException();
			//return Collections.emptyList();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		ResultSet id;
		try {
			PreparedStatement statement =
					connection.prepareStatement("INSERT into teams VALUES(DEFAULT , ?)", Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, team.getName());
			if (statement.executeUpdate() != 1) {
				return false;
			}
			id = statement.getGeneratedKeys();
			if (id.next()) {
				int idField = id.getInt(1);
				team.setId(idField);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException();
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		try {
			connection.setAutoCommit(false);
			PreparedStatement statement = connection.prepareStatement("INSERT INTO users_teams VALUES (?, ?)");
			for(Team t: teams){
				statement.setInt(1, user.getId());
				statement.setInt(2, t.getId());
				statement.addBatch();
			}
			int[] changes = statement.executeBatch();
			for (int change : changes) {
				if (change != 1)
					return false;
			}
			connection.commit();
			return true;
		} catch (SQLException e) {
			try {
				connection.rollback();
			} catch (SQLException e1) {
				e.printStackTrace();
				throw new DBException();
			}
			e.printStackTrace();
			throw new DBException();
		} finally {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		//return false;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		try {
			PreparedStatement statement = connection.prepareStatement("SELECT t.id, t.name FROM users_teams ut\n"
					+ "JOIN users u ON ut.user_id = u.id\n" + "JOIN teams t ON ut.team_id = t.id\n" + "WHERE u.id = ?");
			statement.setInt(1, user.getId());
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()) {
				Team team = new Team();
				team.setId(resultSet.getInt(1));
				team.setName(resultSet.getString(2));
				teams.add(team);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException();
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try {
			PreparedStatement statement = connection.prepareStatement("DELETE FROM users_teams WHERE team_id = ?");
			statement.setInt(1, team.getId());
			if (statement.executeUpdate() == 0) {
				return false;
			}
			statement = connection.prepareStatement("DELETE FROM teams WHERE id = ?");
			statement.setInt(1, team.getId());
			if (statement.executeUpdate() != 1) {
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException();
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		try {
			PreparedStatement statement = connection.prepareStatement("UPDATE teams SET name =? WHERE id = ?");
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			if(statement.executeUpdate() != 1){
				return false;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException();
		}
		return true;
	}

}
